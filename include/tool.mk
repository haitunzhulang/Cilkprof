# CILKRTS_HOME = $(COMPILERS_HOME)
# TOOL_HOME ?= $(HOME)/sandbox/cilktools

# DO NOT USE dynamic linking!  Instrumentation calls get replaced with empty
# tool defined in the runtime if dynamic linking is used.
# flags for for dynamic linking of cilkrts in LDFLAGS
# CILKRTS_DYNAMIC_LIB=-Wl,-rpath -Wl,$(CILKRTS_HOME)/lib -lcilkrts
CILKRTS_STATIC_LIB=$(CILKRTS_HOME)/lib/libcilkrts.a
LDFLAGS = $(CILKRTS_STATIC_LIB) $(CILKTOOL_STATIC_LIB)
LDLIBS= -lrt -ldl -lpthread
TOOLFLAGS += -I$(TOOL_HOME)/include

ifeq ($(TOOL),CILKPROF)
	BASIC_CFLAGS += -DCILKPROF=1 -DCILKSAN=0
	APPFLAGS += -fcilktool -fcilktool-instr-c
        # TOOL_TARGET = $(TOOL_HOME)/cilkprof/cilkprof.o
        TOOL_TARGET = $(TOOL_HOME)/lib/libcilkprof.a
else ifeq ($(TOOL),CILKSAN)
	TOOLFLAGS += -I$(TOOL_HOME)/cilksan -fsanitize=thread
	BASIC_CFLAGS += -DCILKPROF=0 -DCILKSAN=1
        TOOL_TARGET = $(TOOL_HOME)/cilksan/libcilksan.a
	APPFLAGS += -fcilktool
        # Use the following for dynamic linking
	# LDFLAGS += -Wl,-rpath -Wl,$(TOOL_HOME)/cilksan -L$(TOOL_HOME)/cilksan
	# LDLIBS += -lcilksan
else ifeq ($(TOOL),NULL)
	BASIC_CFLAGS += -DCILKPROF=0 -DCILKSAN=0
else
	echo "Error: unrecognized tool $(TOOL)"
endif
